section .data
numbers: db "0123456789"

section .text
 
; Принимает код возврата и завершает текущий процесс
; rdi - код возврата
exit: 
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - адрес строки
string_length:
    push rbp
    mov rbp, rsp
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    mov rsp,rbp
    pop rbp
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - адрес строки
print_string:
    push rbp
    mov rbp, rsp 
    push rax
    push rdi
    call string_length   
    pop rdi 
    mov rsi, rdi         ; string address
    mov rdi, 1           ; stdout descriptor  
    mov rdx, rax         ; string length in bytes
    mov rax, 1           ; 'write' syscall number
    push rcx
    syscall
    pop rcx
    pop rax
    mov rsp,rbp
    pop rbp
    ret


; Принимает код символа и выводит его в stdout
; rdi - код символа
print_char:
    push rbp
    mov rbp, rsp
    push rdi
    mov rsi, rsp
    mov rax, 1 
    mov rdi, 1
    mov rdx, 1
    syscall
   
    pop rdi
   mov rsp,rbp
   pop rbp

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline: 
    mov rax, 1            
    mov rdi, 1            
    mov rsi, 0x10
    mov rdx, 1            
    syscall 
ret


; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - знаковое число
print_int:
    cmp rdi, 0
    jge print_uint ; same as unsigned prettymuch

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - число
print_uint:
push rbp
mov rbp,rsp

    xor r8, r8
    mov r8, 10 ; 
    mov r9, rsp
    mov rax, rdi 
    xor rsi, rsi
    push si ; 
.loop:
    xor rcx, rcx 
    xor rdx, rdx
    div r8
    mov cl, byte[numbers+rdx]
    mov ch, byte[rsp]
    inc rsp
    push cx
    inc rsi
    cmp rax, 0
    jnz .loop

.print:
    mov rdi, rsp
    push r9
    call print_string
    pop rsp
    xor rax, rax

mov rsp,rbp
    pop rbp
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - первая строка
; rsi - вторая строка
string_equals:
push rbp
mov rbp,rsp

    push rbx

    xor rax, rax
.loop:
    mov bl, byte [rsi + rax]
    cmp byte [rdi + rax], bl ; marvelous
    jne .fail

    cmp byte [rdi + rax], 0
    je .succ

    inc rax
    jmp .loop

.fail:
    xor rax, rax
    jmp .end

.succ:
    mov rax, 1

.end:
    pop rbx

mov rsp,rbp
    pop rbp
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
push rbp
mov rbp,rsp

    xor rax, rax
    push rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall

    xor rax, rax
    mov al, byte[rsp]
    pop rdx

mov rsp,rbp
    pop rbp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес буфера
; rsi - размер буфера
read_word: ; fricked up
    xor rcx, rcx 
    dec rsi 
    
    .init:
    push rcx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rcx
    cmp al, 0x20
    je .init
    cmp al, 0x9
    je .init
    cmp al, 0xA
    je .init
    cmp al, 0
    je .end
    mov [rdi], al
    inc rcx
    jmp .loop

    .loop:
    cmp rcx, rsi
    jg .err
    push rcx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rcx
    cmp al, 0x20
    je .end
    cmp al, 0x9
    je .end
    cmp al, 0xA
    je .end
    cmp al, 0
    je .end
    lea rdx, [rdi + rcx]
    mov [rdx], al
    inc rcx
    jmp .loop

    .end:
    lea rdx, [rdi + rcx]
    mov  al, 0
    mov [rdx], al
    mov rax, rdi
    mov rdx, rcx
    ret

    .err:
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
push rbp
mov rbp,rsp
    push rbx

    xor rdx, rdx
    xor rax, rax
    xor rbx, rbx
.loop:                          
    mov bl, byte [rdi + rdx]   
    sub bl, '0'
    jl .end                     
    cmp bl, 9
    jg .end                    

    push rdx
    mov rdx, 10
    mul rdx                     
    pop rdx
    add rax, rbx                

    inc rdx                     
    jmp .loop                

.end:
    pop rbx
    mov rsp,rbp
    pop rbp
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi - адрес начала строки
parse_int: 
    xor rcx, rcx
    mov cl, [rdi]
    cmp cl, '-'
    je .neg
    jmp parse_uint

    .neg:
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    cmp rdx, 0
    je .err
    inc rdx
    neg rax
    ret

    .err:
    xor rdx, rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rsi - указатель на буфер
; rdi - указатель на строку
; rdx - длина буфера
string_copy:
	push rbp
	mov rbp, rsp
    push rbx
    xor rax, rax

.loop:
    cmp rax, rdx
    je .fail

    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0
    je .end

    inc rax
    jmp .loop

.fail:
    xor rax, rax

.end:
    pop rbx
    mov rsp,rbp
    pop rbp
    ret


